FROM openjdk:11
EXPOSE 8080
WORKDIR /app
COPY ./build/libs/*.jar /app/paopao.jar
# COPY ./*.jar /app/paopao.jar
COPY ./.env.develop /app/.env.develop
COPY ./archive /app/archive
COPY ./thirdparty /app/thirdparty
ENTRYPOINT [ "java","-jar","paopao.jar" ]
